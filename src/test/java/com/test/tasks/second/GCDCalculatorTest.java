package com.test.tasks.second;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GCDCalculatorTest {

    private GCDCalculator gcdCalculator;

    @Before
    public void setUp() throws Exception {
        gcdCalculator = new GCDCalculator();
    }

    @Test
    public void calculateGcdIteration() {
        int a = 36;
        int b = 18;
        int gcd = gcdCalculator.calculateGcdIteration(a, b);
        assertEquals("Expected GCD = 18", 18, gcd);
    }

    @Test
    public void calculateGcdRecursion() {
        int a = 36;
        int b = 18;
        int gcd = gcdCalculator.calculateGcdRecursion(a, b);
        assertEquals("Expected GCD = 18", 18, gcd);
    }
}