package com.test.tasks.third;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RunnerTest {

    private Runner runner;

    @Before
    public void setUp() throws Exception {
        runner = new Runner();
    }

    @Test
    public void countWordInText() {
        String text = "Text for test, text for test. Text for test";
        String word = "test";
        long amountWord = runner.countWordInText(word, text);
        assertEquals("Expected amount word \"test\" in text = 3", 3, amountWord);
    }
}