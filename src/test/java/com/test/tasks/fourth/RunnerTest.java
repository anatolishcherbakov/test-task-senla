package com.test.tasks.fourth;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class RunnerTest {

    private Runner runner;
    private List<Item> items;
    private List<Item> expectedItemsInKnapsack;

    @Before
    public void setUp() throws Exception {
        runner = new Runner();
        Item item1 = new Item(1, 5, 10);
        Item item2 = new Item(2, 4, 40);
        Item item3 = new Item(3, 6, 30);
        Item item4 = new Item(4, 3, 50);
        items = new ArrayList<>(Arrays.asList(item1, item2, item3, item4));
        expectedItemsInKnapsack = new ArrayList<>(Arrays.asList(item4, item2));
    }

    @Test
    public void getItemsWithMaxTotalValue() {
        List<Item> itemsInKnapsack = runner.getItemsWithMaxTotalValue(items, 10);
        assertEquals(expectedItemsInKnapsack, itemsInKnapsack);
    }
}