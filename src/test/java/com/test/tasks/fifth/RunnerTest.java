package com.test.tasks.fifth;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RunnerTest {
    private Runner runner;
    private int[][] array;

    @Before
    public void setUp() throws Exception {
        runner = new Runner();
        array = new int[][]{{3, 2, 1}, {5, 7, 3}, {9, 3, 6}};
    }

    @Test
    public void sortElementsOfRowByAscendingAndRowsByDescendingSumOfElementsInRow() {
      int[][] a = new int[][]{{3, 6, 9}, {3, 5, 7}, {1, 2, 3}};
      runner.sortElementsOfRowByAscendingAndRowsByDescendingSumOfElementsInRow(array);
      assertArrayEquals(a, array);
    }
}