package com.test.tasks.first;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class RunnerTest {

    private Runner runner;

    @Before
    public void setUp() throws Exception {
        runner = new Runner();
    }

    @Test
    public void testForIntegerCompositeEvenNumber() {
        int x = 36;
        String expectedMessage = "You number is integer, composite, even.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

    @Test
    public void testForIntegerCompositeOddNumber() {
        int x = 27;
        String expectedMessage = "You number is integer, composite, odd.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

    @Test
    public void testForIntegerPrimeEvenNumber() {
        int x = 2;
        String expectedMessage = "You number is integer, prime, even.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

    @Test
    public void testForIntegerPrimeOddNumber() {
        int x = 29;
        String expectedMessage = "You number is integer, prime, odd.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

    @Test
    public void testForIntegerEvenNumber() {
        int x = 0;
        String expectedMessage = "You number is integer, even.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

    @Test
    public void testForIntegerOddNumber() {
        int x = 1;
        String expectedMessage = "You number is integer, odd.";
        String message = runner.defineSetsForNumber(x);
        assertEquals("Expected next string: " + expectedMessage, expectedMessage, message);
    }

}