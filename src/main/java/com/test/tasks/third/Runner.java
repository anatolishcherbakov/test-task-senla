package com.test.tasks.third;

import java.util.Arrays;
import java.util.Scanner;

public class Runner {

    /*Создать программу, которая подсчитывает сколько раз употребляется слово в тексте (без учета регистра).
    Текст и слово вводится вручную.*/

    public static void main(String[] args) {
        Runner runner = new Runner();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a text please -> ");
        String text = scanner.nextLine();
        System.out.print("Enter a word for find please -> ");
        String word = scanner.nextLine();

        long amountWord = runner.countWordInText(word, text);

        System.out.println("Amount words " + "<<" + word + ">> in text = " + amountWord);
    }

    public long countWordInText(String word, String text) {
        String[] words = text.split(" ");
        return  Arrays.stream(words)
                .filter(w -> w.equalsIgnoreCase(word)
                        || w.equalsIgnoreCase(word + ",")
                        || w.equalsIgnoreCase(word + "."))
                .count();
    }
}
