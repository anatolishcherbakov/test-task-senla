package com.test.tasks.fourth;

import java.util.List;

public class Knapsack {
    private int capacity;
    private List<Item> items;

    public Knapsack(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Knapsack{" +
                "items=" + items +
                '}';
    }
}
