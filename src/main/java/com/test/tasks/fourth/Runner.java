package com.test.tasks.fourth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Runner {

   /* Имеется набор вещей, которые необходимо поместить в рюкзак. Рюкзак обладает заданной грузоподъемностью.
    Вещи в свою очередь обладают двумя параметрами — весом и стоимостью. Цель задачи заполнить рюкзак
    не превысив его грузоподъемность и максимизировать суммарную ценность груза.
    Входные данные: грузоподъёмность рюкзака, N - кол-во вещей, каждая последущая строка X Y,
    где X это вес, а Y стоимость вещи. Вывод, список вещей (X Y) через запятую, который даст
    максимальную стоимость. Для простоты используем целочисленные значения для веса и стоимости.*/

    public static void main(String[] args) {
        Runner runner = new Runner();
        Item item1 = new Item(1,5, 10);
        Item item2 = new Item(2,4, 40);
        Item item3 = new Item(3,6, 30);
        Item item4 = new Item(4,3, 50);
        List<Item> items = new ArrayList<>(Arrays.asList(item1, item2, item3, item4));

        Knapsack knapsack = new Knapsack(10);
        int knapsackCapacity = knapsack.getCapacity();

        items = runner.getItemsWithMaxTotalValue(items, knapsackCapacity);
        knapsack.setItems(items);

        System.out.println("Items into knapsack: " + knapsack.getItems());
    }

    public  List<Item> getItemsWithMaxTotalValue(List<Item> list, int knapsackCapacity) {
        int n = list.size();
        int[] W = new int[n];
        int[] V = new int[n];

        for (int i = 0; i < n; i++) {
            Item item = list.get(i);
            W[i] = item.getWeight();
            V[i] = item.getValue();
        }

        int[][] B = new int[n + 1][knapsackCapacity + 1];

        for (int i = 0; i <= n; i++)
            for (int j = 0; j <= knapsackCapacity; j++) {
                B[i][j] = 0;
            }

        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= knapsackCapacity; j++) {
                B[i][j] = B[i - 1][j];

                if ((j >= W[i - 1]) && (B[i][j] < B[i - 1][j - W[i - 1]] + V[i - 1])) {
                    B[i][j] = B[i - 1][j - W[i - 1]] + V[i - 1];
                }
            }
        }

        List<Item> items = new ArrayList<>();
        while (n != 0) {
            if (B[n][knapsackCapacity] != B[n - 1][knapsackCapacity]) {
                items.add(list.get(n - 1));
                knapsackCapacity = knapsackCapacity - W[n - 1];
            }

            n--;
        }
        return items;
    }
}
