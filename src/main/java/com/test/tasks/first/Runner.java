package com.test.tasks.first;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {

        /*Создать программу, которая будет сообщать, является ли целое число, введенное пользователем,
        чётным или нечётным, простым или составным. Если пользователь введёт не целое число, то сообщать
        ему об ошибке.*/

    public static void main(String[] args) {
        Runner runner = new Runner();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an integer please -> ");
        try {
            int x = scanner.nextInt();
            String message = runner.defineSetsForNumber(x);
            System.out.println(message);

        } catch (InputMismatchException e) {
            System.out.println("Error: You entered not integer!");
        }
    }

    public String defineSetsForNumber(int x) {
        String message = "You number is integer, ";

        if (x > 1) {
            for (int i = 2; i < x; i++) {
                if (x % i == 0) {
                    message += "composite, ";
                    break;
                }
            }
            if (!message.contains("composite")) {
                message += "prime, ";
            }
        }
        message += x % 2 == 0 ? "even." : "odd.";

        return message;
    }
}
