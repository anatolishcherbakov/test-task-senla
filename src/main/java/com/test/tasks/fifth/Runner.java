package com.test.tasks.fifth;

import java.util.Arrays;
import java.util.Scanner;

public class Runner {

    /*Имеется 2ух-мерный целочисленный массив, для простоты NxN. Вводитеся размерность N,
    а после N строк с элементами через зяпятую или пробел. Отсортировать елементы строк по возрастанию,
    а строки по убыванию суммы элементов в строке. Найти максимальный элемент.*/

    public static void main(String[] args) {
        Runner runner = new Runner();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter N for two-dimensional array NxN -> ");
        int N = scanner.nextInt();
        int[][] array = new int[N][N];

        for (int i = 0; i < N; i++) {
            Scanner scan = new Scanner(System.in);
            int rowNumber = i + 1;
            System.out.print("Enter " + rowNumber + " row of integers separated by space -> ");
            String s = scan.nextLine();

            String[] stringIntegers = s.split(" ");
            for (int j = 0; j < stringIntegers.length; j++) {
                int x = Integer.parseInt(stringIntegers[j]);
                array[i][j] = x;
            }
        }

        runner.sortElementsOfRowByAscendingAndRowsByDescendingSumOfElementsInRow(array);
        System.out.println(Arrays.deepToString(array));
    }

    public void sortElementsOfRowByAscendingAndRowsByDescendingSumOfElementsInRow(int[][] array) {
        int N = array.length;

        int[] rowSum = new int[N];

        for (int i = 0; i < N; i++) {

            Arrays.sort(array[i]);

            for (int j = 0; j < array[i].length; j++) {
                int x = array[i][j];
                rowSum[i] += x;
            }

            if (i != 0) {
                for (int k = 0; i - k > 0 && rowSum[i - k] > rowSum[i - k - 1]; k++) {
                    int[] rowTemporary = array[i - k - 1];
                    array[i - k - 1] = array[i - k];
                    array[i - k] = rowTemporary;

                    int sumTemporary = rowSum[i - k - 1];
                    rowSum[i - k - 1] = rowSum[i - k];
                    rowSum[i - k] = sumTemporary;
                }
            }
        }
    }
}
