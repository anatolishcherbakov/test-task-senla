package com.test.tasks.second;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {

    /*Создать программу, которая будет вычислять и выводить на экран НОД (наибольший общий делитель)
    двух целых чисел, введенных пользователем. Сделать в 2 вариантах, с рекурсией и без рекурсии.
    Если пользователь некорректно введёт хотя бы одно из чисел, то сообщать об ошибке.*/

    public static void main(String[] args) {
        GCDCalculator gcdCalculator = new GCDCalculator();
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Enter first integer please -> ");
            int a = scanner.nextInt();
            System.out.print("Enter second integer please -> ");
            int b = scanner.nextInt();

            int gcdIteration = gcdCalculator.calculateGcdIteration(a, b);
            int gcdRecursion = gcdCalculator.calculateGcdRecursion(a, b);

            System.out.println("\"Iteration\" GCD for " + a + " and " + b + " = " + gcdIteration);
            System.out.println("\"Recursion\" GCD for " + a + " and " + b + " = " + gcdRecursion);

        } catch (InputMismatchException e) {
            System.out.println("Error: You entered not integer!");
        }
    }
}
