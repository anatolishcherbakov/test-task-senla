package com.test.tasks.second;

public class GCDCalculator {

    public int calculateGcdIteration(int a, int b) {
        int x;
        while (b != 0) {
            x = b;
            b = a % b;
            a = x;
        }
        return Math.abs(a);
    }

    public int calculateGcdRecursion(int a, int b) {
        if (b == 0) return a;
        return Math.abs(calculateGcdRecursion(b, a % b));
    }
}
